from models import Product, Category
from news.models import New

def base(request):
    return {
        "category_list": Category.objects.order_by("order", "name"),
        "side_menu_category_list": Category.objects.filter(show_in_side_menu=True).order_by("order", "name"),
        "new_list": New.objects.order_by("-pk"),
    }
from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.utils.encoding import force_unicode

from image_cropping.fields import ImageRatioField, ImageCropField
# {% thumbnail new.img 143x115 box=new.img_crop crop detail %}

class Category(models.Model):
    order = models.IntegerField(default=0, max_length=2)
    name = models.CharField(max_length=255)
    url_key = models.CharField(max_length=255, unique=True, blank=True)
    img = ImageCropField(upload_to='uploaded_images', blank=True, null=True)
    img_crop = ImageRatioField('img', '960x300')
    description = models.TextField(blank=True, null=True)

    show_in_side_menu = models.BooleanField()

    def __unicode__(self):
        return force_unicode("%s " % (self.name))

    def save(self, *args, **kwargs):
        if not self.url_key:
            self.url_key = slugify(self.name)
        return super(Category, self).save(*args, **kwargs)

class Subcategory(models.Model):
    category = models.ForeignKey(Category)

    order = models.IntegerField(default=0, max_length=2)
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return force_unicode("%s " % (self.name))

class Article(models.Model):
    subcategory = models.ForeignKey(Subcategory)

    order = models.IntegerField(default=0, max_length=2)
    name = models.CharField(max_length=255)
    img = ImageCropField(upload_to='uploaded_images', blank=True, null=True)
    img_crop = ImageRatioField('img', '400x200')
    description = models.TextField()

    def __unicode__(self):
        return force_unicode("%s %s" % (self.subcategory.name, self.description[:20]))


class Product(models.Model):
    category = models.ForeignKey(Category, blank=True, null=True)

    order = models.IntegerField(default=0, max_length=2)
    name = models.CharField(max_length=255)
    url_key = models.CharField(max_length=255, unique=True, blank=True)
    img = ImageCropField(upload_to='uploaded_images', blank=True, null=True)
    img_crop = ImageRatioField('img', '400x200')
    description = models.TextField()
    link_to_shop = models.URLField(null=True, blank=True)

    def __unicode__(self):
        return force_unicode("%s " % (self.name))

    def save(self, *args, **kwargs):
        if not self.url_key:
            self.url_key = slugify(self.name)
        return super(Product, self).save(*args, **kwargs)

class Pdf(models.Model):
    product = models.ForeignKey(Product)

    name = models.CharField(max_length=32)
    pdf_file = models.FileField(upload_to="pdf")

    def __unicode__(self):
        return force_unicode("%s" % ( self.name))

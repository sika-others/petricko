from django.contrib import admin

from models import Product, Category, Subcategory, Article, Pdf

class ProductAdmin(admin.ModelAdmin):
    model = Product
    ordering = ("name", )

class ArticleAdmin(admin.ModelAdmin):
    model = Article
    ordering = ("name", )

class SubcategoryAdmin(admin.ModelAdmin):
    model = Subcategory
    ordering = ("name", )
        
class CategoryAdmin(admin.ModelAdmin):
    model = Category
    ordering = ("name", )

class PdfAdmin(admin.ModelAdmin):
    model = Pdf
    ordering = ("name", )


admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Subcategory, SubcategoryAdmin)
admin.site.register(Article, ArticleAdmin)
admin.site.register(Pdf, PdfAdmin)
from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.core.urlresolvers import reverse

from models import Product, Category


def category_view(request, category_url_key, template="petricko/category.html"):
    obj = get_object_or_404(Category, url_key=category_url_key)
    products = obj.product_set.all().order_by("order", "name")
    subcategory_list = obj.subcategory_set.all().order_by("order", "name")
    if subcategory_list.count():
        template = "petricko/category_sub.html"
    return render_to_response(template,
        {"category":obj,
         "products_list": products,
         "subcategory_list": subcategory_list, },
        context_instance=RequestContext(request))
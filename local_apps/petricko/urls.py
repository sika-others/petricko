from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import direct_to_template

from news.views import new_view, news_list_view
from views import category_view

urlpatterns = patterns('',
    url(r'^novinka/(?P<new_url_key>[a-zA-Z0-9-]+)$',
        new_view,
        {"template": "petricko/new.html"},
        name="petricko.new", ),
    url(r'^(?P<category_url_key>[a-zA-Z0-9-]+)$',
        category_view,
        name="petricko.category", ),
)
from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.core.urlresolvers import reverse

from models import New

def new_view(request, new_url_key, template):
    obj = get_object_or_404(New, url_key=new_url_key)
    return render_to_response(template,
        {"new":obj},
        context_instance=RequestContext(request))

def news_list_view(request, template):
    objs = New.objects.order_by("-pk")
    return render_to_response(template,
        {"news_list":objs},
        context_instance=RequestContext(request))
from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.utils.encoding import force_unicode

from image_cropping.fields import ImageRatioField, ImageCropField
# {% thumbnail new.img 143x115 box=new.img_crop crop detail %}

class New(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=255)
    url_key = models.CharField(max_length=255, unique=True, blank=True)
    img = ImageCropField(upload_to='uploaded_images', blank=True, null=True)
    img_crop = ImageRatioField('img', '143x115')
    description = models.TextField()
    content = models.TextField(null=True, blank=True)

    def get_created_at(self):
        return self.created_at.strftime('%e. %m. %Y')

    def __unicode__(self):
        return force_unicode("%s " % (self.name))

    def save(self, *args, **kwargs):
        self.url_key = slugify(self.name)
        return super(New, self).save(*args, **kwargs)
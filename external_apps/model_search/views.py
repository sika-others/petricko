from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.core.urlresolvers import reverse

from __init__ import search

def search_results_view(request, template="model_search/search.html"):
    try:
        response = search(request.GET["q"].replace("+", " "))
        if not request.GET["q"]:
            response = []
    except KeyError:
        response = []

    results_list = response
    return render_to_response(template,
        {"results_list": results_list, },
        context_instance=RequestContext(request))
===================
Django Model search
===================
:Author: Ondrej Sika <sika.ondrej@gmail.com>

About
=====

App for fulltext searching in Djnago models


Installation
============

Copy all files to dir model_search place for Django apps in your project.

Add model_search to INSTALLED_APPS

Add to urls.py to urlpatterns ("^search$", "model_search.views.search_results_view"),

For searching must be defined whitch app, model and field may by browsed to 
MODEL_SEARCH_MAP in settings.py

Example
=======

Model search map

>>> MODEL_SEARCH_MAP = (
>>>     ("app1", (
>>>         ("Model1", ("name", "description",)),
>>>     )),
>>>     ("app2", (
>>>         ("Model1", ("title", "content",)),
>>>         ("Model2", ("name", "url",)),
>>>     )),
>>> ) 

Example of template for result view is located in templates/model_search/search.html
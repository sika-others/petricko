from conf import settings


def search(term):
    results = []
    for (app, array) in settings.MODEL_SEARCH_MAP:
        try:
            for (model, array) in array:
                exec "from %s.models import %s" % (app, model)
                exec "objs = %s.objects.all()" % (model)
                res = ()
                for field in array:
                    exec "res += tuple(objs.filter(%s__contains=term))" % (field)
                ret = (app, model, list(set(res)))
                if res:
                    results.append(ret)
        except ValueError:
            pass
    return results
# -*- coding: utf-8 -*-
# Django void CMS v.1.0 for Django 1.4 (fork of Django void v.1.0)
# author:   Ondrej Sika
#           sika.ondrej@gmail.com
#           http://ondrejsika.com

from _urls import *

# CMS urls
urlpatterns += patterns('',
    url(r'^', include('cms.urls')),
)

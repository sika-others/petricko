# -*- coding: utf-8 -*-
# Django void CMS v.1.0 for Django 1.4 (fork of Django void v.1.0)
# author:   Ondrej Sika
#           sika.ondrej@gmail.com
#           http://ondrejsika.com


from _settings import *


INSTALLED_APPS += (
    'cms',                      # django CMS itself
    'mptt',                     # utilities for implementing a modified pre-order traversal tree
    'menus',                    # helper for model independent hierarchical website navigation
    'sekizai',                  # for javascript and css management
    'south',                    # for migration database

    'cms.plugins.file',
    'cms.plugins.flash',
    'cms.plugins.googlemap',
    'cms.plugins.link',
    'cms.plugins.picture',
    'cms.plugins.snippet',
    'cms.plugins.teaser',
    'cms.plugins.text',
    'cms.plugins.video',
    'cms.plugins.twitter',

    "reversion",
)

MIDDLEWARE_CLASSES += (
    'cms.middleware.multilingual.MultilingualURLMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS += (
    'cms.context_processors.media',
    'sekizai.context_processors.sekizai',
)

CMS_TEMPLATES = (
    ('blank_template.html', 'Blank template'),
)

LANGUAGES = [
    ('en', 'English'),
]
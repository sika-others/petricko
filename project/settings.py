# -*- coding: utf-8 -*-
# Django void CMS v.1.0 for Django 1.4 (fork of Django void v.1.0)
# author:   Ondrej Sika
#           sika.ondrej@gmail.com
#           http://ondrejsika.com


from _settings import *

# CMS settings
from _settings_cms import *


# libs
INSTALLED_APPS += (
    "django.contrib.markup",

    "south",
    "easy_thumbnails",
)

# external apps
INSTALLED_APPS += (
    "news",
    "model_search",
    "image_cropping",
    "poll",
    "form_designer",
    "picklefield",
    'form_designer.contrib.cms_plugins.form_designer_form',
)

# local apps
INSTALLED_APPS += (
    "petricko",
)

# Django settings
DEBUG = False
TEMPLATE_CONTEXT_PROCESSORS += (
    "petricko.context_processors.base",
) 

# Database setting
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',  # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'petricko',                      # Or path to database file if using sqlite3.
        'USER': 'petricko',                      # Not used with sqlite3.
        'PASSWORD': 'michael123',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}


# Mail settings # Default mail settings for gmail
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'mail@gmail.com'
EMAIL_HOST_PASSWORD = '***'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
SERVER_EMAIL = EMAIL_HOST_USER
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER

# CMS settings
CMS_TEMPLATES = (
    ('petricko/cms/static_page.html', 'Static page'),
    ('petricko/cms/home_page.html', 'Home page'),
    ('petricko/cms/with_sidebar.html', 'With sidebar'),
)
MIDDLEWARE_CLASSES.remove('cms.middleware.multilingual.MultilingualURLMiddleware')
LANGUAGES = (("cs", "Czech"), )
LANGUAGE_CODE = "cs"
# libs settings
THUMBNAIL_PROCESSORS = ('image_cropping.thumbnail_processors.crop_corners', 'easy_thumbnails.processors.colorspace', 'easy_thumbnails.processors.autocrop', 'easy_thumbnails.processors.scale_and_crop', 'easy_thumbnails.processors.filters')

MODEL_SEARCH_MAP = (
    ("news", (
        ("New", ("name", "description",)),
    )),
    ("petricko", (
        ("Category", ("name", "description",)),
        ("Article", ("name", "description",)),
        ("Product", ("name", "description",)),
        ("Subcategory", ("name", )),
    )),
)

# external apps settings
JQUERY_JS = "http://code.jquery.com/jquery.min.js"


# local apps settings

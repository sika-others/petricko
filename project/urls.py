# -*- coding: utf-8 -*-
# Django void CMS v.1.0 for Django 1.4 (fork of Django void v.1.0)
# author:   Ondrej Sika
#           sika.ondrej@gmail.com
#           http://ondrejsika.com

from _urls import *

# CMS urls
from _urls_cms import *

# libs urls
urlpatterns += patterns('', 
    url(r'^poll/', include('poll.urls')),
)

# external apps urls
urlpatterns += patterns('', 
    ("^search$", "model_search.views.search_results_view", {"template": "petricko/search.html"}),
    (r'^forms/', include('form_designer.urls')),
)

# local apps urls
urlpatterns += patterns('', 
    ("^", include("petricko.urls")),
)